# Description
---
A web application that allows the Filipino citizens to design and suggest road construction projects that they would like to see implemented in the country. It will be an avenue for the politicians to know what the people truly want.


# Features
---
- Add a suggestion
- Map Marker
- Category
- Short Description
- Status
- Upvote/Downvote
- Comments
- Link to Social Websites
    - Facebook
    - Google Account

