<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hoy! Ayos</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">
    <link href="css/landingpage.css" rel="stylesheet">
    <link href="css/cloud.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="css/style.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
                <a href="#top"  onclick = $("#menu-close").click(); >HOY! AYOS</a>
            </li>
            <li>
                <a href="#top" onclick = $("#menu-close").click(); >Home</a>
            </li>
            <li>
                <a href="#footer" onclick = $("#menu-close").click(); >About</a>
            </li>
            <li>
                <a href="#map2" onclick = $("#menu-close").click(); >Map</a>
            </li>
            <li>
                <a href="#trending" onclick = $("#menu-close").click(); >Trending Projects</a>
            </li>
        </ul>
    </nav>

    <!-- Header -->

    <section id="top">
      <div class="row cityscape">
        <div class="random-container">
          <!--<div id="clouds">
          	<img class="cloud x1" src="img/cloud.png" alt="Cloud">
          	<img class="cloud x2" src="img/cloud.png" alt="Cloud">
            <img class="cloud x3" src="img/cloud.png" alt="Cloud">
            <img class="cloud x4" src="img/cloud.png" alt="Cloud">
            <img class="cloud x5" src="img/cloud.png" alt="Cloud">
          </div>-->

          <div id="random-text" class="row text-center" style="padding-top:5px;">
              <a href="#footer"><img class = "logo" src = "img/logo.png" alt = "Logo" style="width:20%"></a>
              <h1 style="margin-bottom:0px; margin-top:0px"></h1>
            <div class="col-md-4 col-md-offset-4">
              <h4>There are many ongoing road works around the country. But there are just as much roads that need fixing.</h4>
              <a class="btn btn-default" href="#map2">Then Mark it!</a>
            </div>
          </div>
        </div>

        <div class="buildings">
          <img class="img bridge animated fadeInUpBig" src="img/bridge.png" alt="Bridge">
          <img class="img blue-building animated fadeInUpBig" src="img/blue-building.png" alt="Building">
          <img class="img yellow-building animated fadeInUpBig" src="img/yellow-building.png" alt="Building">
          <img class="img gray-building animated fadeInUpBig" src="img/gray-building.png" alt="Building">
          <img class="img flyover animated fadeInUpBig" src="img/flyover.png" alt="flyover">
          <img class="img green-building animated fadeInUpBig" src="img/green-building.png" alt="Building">
		  <img class="img blue-building2 animated fadeInUpBig" src="img/blue-building.png" alt="Building">
		  <img class="img gray-building2 animated fadeInUpBig" src="img/gray-building.png" alt="Building">
          <img src="img/road-horizontal.png" alt="Building" style="width:100%">
        </div>
      </div>
    </section>

    <section>
      <div class="row">
        <img src="img/road.png" alt="road" style="width:100%">
      </div>
    </section>

    <!-- SUGGEST PAGE -->
    <section id="map2" class="map2" style = "background-color:#2C3E50;">
        <div class="container">
            <div class="row">
                <div class = "col-md-8" style="padding-top:30px; padding-bottom:30px">
                    <?php include("maps.php"); ?>
                </div>
                <div class = "col-md-4" style="padding-top:45px; padding-bottom:30px">
                    <div class = "row">
                        <img class = "img balloon" src = "img/balloon.png" alt = "Speech" width = "100%" style = "float:left;padding-top:12vh">
                    </div>
                    <div class ="row">
                        <img class = "img girl" src = "img/girl.png" alt = "Girl" width = "140" style="float:right;">
                        <a class="btn btn-default" href="#trending">Trending Projects</a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Trending -->
    <section id="trending">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2>Trending Projects</h2>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-road fa-stack-1x text-primary"></i>
                            </span>
                                <h4>Road is very bumpy due to overuse
                                </h4>
                                <p>Likes: 360,908</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-compass fa-stack-1x text-primary"></i>
                            </span>
                                <h4>Several holes on the path that need to be filled. Reconstruction would be ideal</h4>
                                <p>Likes: 78,345</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-road fa-stack-1x text-primary"></i>
                            </span>
                                <h4>Baku-bako na yung sidewalk dito. Delikado na. Baka naman pwedeng ipaayos</h4>
                                <p>Likes: 333,999</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-car fa-stack-1x text-primary"></i>
                            </span>
                                <h4>Maganda sigurong lagyan ng footbridge dito. Ang haba na nga ng tatawirin ang bilis pa ng mga sasakyan</h4>
                                <p>Likes: 999,901</p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- ABOUT PAGE -->
    <section id = "footer">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h2 style = "font-size:50px; text-align:right; margin-bottom:10px"><strong>
                        ABOUT</strong></h2>
                    <h2 style = "font-size:50px; text-align:right; margin-top:8px"><strong>
                        HOY!AYOS</strong><h2>
                </div>
                <div class = "col-md-7">
                    <h2 style = "font-size:22px; text-align:right;">
                        <strong>"Hoy!Ayos"</strong> is a web application that allows the Filipino citizens to design and suggest
                        road construction projects that they would like to see implemented in the country.
                        It will serve as an avenue for the government to know what the people truly want.
                    </h2>
                </div>
                <div class = "col-md-2">
                    <h4 style = "font-size:20px; text-align:right; color:black;">
                        <strong>Developers:</strong><br>
                        Antonio, Abi<br>
                        Manuzon, Jojay<br>
                        Marquez, Kirzten<br>
                        Olmilla, Grizel<br>
                        Truelen, Dom
                    </h4>
                </div>
            </div>
        </div>
    </footer>
</section>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    </script>

</body>

</html>
