<!--<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<title>Simple markers</title>
		<style>
			html, body {
			height: 100%;
			margin: 0;
			padding: 0;
			}
			#map {
			height: 100%;
			}
		</style>
	</head>

	<body>-->
		<div id="map"  style = "width:750px; height:650px"></div>
		<?php
			$markers = [];
		$myfile = fopen("data", "r") or die("Unable to open file!");
		while ($temp = fgets($myfile))
		{
			$temp1 = explode("|", $temp);
			array_push($markers, $temp1);
		}
		fclose($myfile);
	?>

	<script>
		var previnfowindow = false;
		var currmarker = false;
		var markers = {};
		var up = {};
		var down = {};
		var type = {};

		function initMap()
		{
			var myLatLng = {lat: 14.648685, lng: 121.068606};

			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 14,
				center: myLatLng
			});

			google.maps.event.addListener(map, 'click', function(event) {
				placeMarker(event.latLng, map);
			});

			var data = <?php echo json_encode($markers) ?>;

			var arrayLength = data.length;

			for (var i = 0; i < arrayLength; i++)
			{
				if (parseInt(data[i][5]) != 0)
				{
					var marker = new google.maps.Marker({
						position: {lat: parseFloat(data[i][0]), lng: parseFloat(data[i][1])},
						map: map,
						icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
					});
					var contentString = "IN PROGRESS<p>" + data[i][2] + "</p>" + "<button class='fa fa-thumbs-up' onclick='javascript:upVote()''> " + data[i][3] +"</button> <button class='fa fa-thumbs-down' onclick='javascript:downVote()'> "+ data[i][4] +"</button>"+  "<br>Price " + data[i][6] + "<br>Start Date " + data[i][7] + "<br>Expected Completion Date " + data[i][8] + "<br>Sponsor " + data[i][9];
				}
				else
				{
					var marker = new google.maps.Marker({
						position: {lat: parseFloat(data[i][0]), lng: parseFloat(data[i][1])},
						map: map
					});
					var contentString = "SUGGESTION<p>" + data[i][2] + "</p>" + "<button class='fa fa-thumbs-up' onclick='javascript:upVote()''> " + data[i][3] +"</button> <button class='fa fa-thumbs-down' onclick='javascript:downVote()'> "+ data[i][4]+"</button>";

				}

				if (data[i][10])
				{
					j = 10;
					contentString += "<br>Comments<br>";
					while (data[i][j])
					{
						contentString += data[i][j] + "<br>" + data[i][j+1] + "<br>";
						j += 2;
					}

				}

				infowindow = new google.maps.InfoWindow({
					content: contentString
				});

				function listen(marker, infowindow)
				{
					marker.addListener('click', function() {
						try {
							previnfowindow.close();
						}
						catch (err){

						}
						infowindow.open(map, marker);
						previnfowindow = infowindow;
						currmarker = marker;
					});
				}

				listen(marker, infowindow);

				markers[marker] = infowindow;
				up[marker] = data[i][3];
				down[marker] = data[i][4];
				type[marker] = data[i][5];
			}
		}

		function placeMarker(location, map) {
			var contentString = "<form action='javascript:changeText();' method='post'>" +
			  "<textarea cols='40' rows='5' name='myname' id='text' placeholder='What do you think this place needs?'></textarea><br>" +
			  "<div class='col-md-4 col-md-offset-4'><input class='btn btn-default' type='submit' value='submit'></div>" +
			  "</form>";

			var infowindow = new google.maps.InfoWindow({
				content: contentString
			});

			var marker = new google.maps.Marker({
				position: location,
				map: map,
				//icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
			});

			marker.addListener('click', function() {
				try {
					previnfowindow.close();
				}
				catch (err){

				}
				infowindow.open(map, marker);
				previnfowindow = infowindow;
				currmarker = marker;

			});

			markers[marker] = infowindow;
			up[marker] = 0;
			down[marker] = 0;
			type[marker] = 0;

			try {
				previnfowindow.close();
			}
			catch (err){

			}
			infowindow.open(map, marker);
			previnfowindow = infowindow;
			currmarker = marker;
		}

		function changeText()
		{
			var infowindow = previnfowindow;
			var marker = currmarker;
			if (type[marker] != 0)
			{
				infowindow.setContent("IN PROGRESS<p>" + document.getElementById('text').value + "</p>" + "<button class='fa fa-thumbs-up' onclick='javascript:upVote()''> " + up[marker] +"</button> <button class='fa fa-thumbs-down' onclick='javascript:downVote()'> "+ down[marker]+"</button>"+ down[marker]);
			}
			else
			{
				infowindow.setContent("SUGGESTION<p>" + document.getElementById('text').value + "</p>" + "<button class='fa fa-thumbs-up' onclick='javascript:upVote()''> " + up[marker] +"</button> <button class='fa fa-thumbs-down' onclick='javascript:downVote()'> "+ down[marker]+"</button>"+ down[marker]);
			}
		}

		function upVote()
		{
			up[currmarker] += 1;
			var infowindow = previnfowindow;
			var marker = currmarker;
			var str = infowindow.content.substr(infowindow.content.indexOf("<p>") + 3, infowindow.content.lastIndexOf("</p>") - 3);

			infowindow.setContent("<p>" + str + "</p>" + "<button class='fa fa-thumbs-up' onclick='javascript:upVote()''> " + up[marker] +"</button> <button class='fa fa-thumbs-down' onclick='javascript:downVote()'> "+ down[marker]+"</button>");
		}

		function downVote()
		{
			down[currmarker] += 1;
			var infowindow = previnfowindow;
			var marker = currmarker;
			var str = infowindow.content.substr(infowindow.content.indexOf("<p>") + 3, infowindow.content.lastIndexOf("</p>") - 3);

			infowindow.setContent("<p>" + str + "</p>" + "<button class='fa fa-thumbs-up' onclick='javascript:upVote()''> " + up[marker] +"</button> <button class='fa fa-thumbs-down' onclick='javascript:downVote()'> "+ down[marker]+"</button>");
		}

	</script>
	<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCixls_nHZRUkGu83dPNHAu4f1u-wC7dKk&signed_in=true&callback=initMap"></script>
<!--	</body>
</html>-->
